export class Course {
  id: number;
  naam: string;
  attitude: string;
  functieniveau: string;
  slagingscriterium: string;
  status: string;
  maxdeelnemers: number;
  beschrijving: string;
  cursusdata: [{
    id: number;
    datum: string;
  }];
  cursusdocenten: [{
    id: number;
    naam: string;
  }];

}
