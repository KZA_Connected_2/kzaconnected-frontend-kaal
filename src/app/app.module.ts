import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SuiModule } from 'ng2-semantic-ui';
import { TrainingComponent } from './training/training.component';
import { TrainingFilterBarComponent } from './training/training-filter-bar/training-filter-bar.component';
import { CoursesService } from './services/courses.service';
import { CourseDataResolver } from './services/routeprovider.service';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  {path: '', component: TrainingComponent, resolve: {courses: CourseDataResolver}}
];

@NgModule({
  declarations: [
    AppComponent,
    TrainingComponent,
    TrainingFilterBarComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    SuiModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
  ],
  providers: [
    CoursesService,
    CourseDataResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
