
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Course} from '../model/courses.model';
import {CoursesService} from './courses.service';

@Injectable()
export class CourseDataResolver implements Resolve<Course> {

  constructor(private readonly colleagueService: CoursesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Course> | Promise<Course> | Course {
    return this.colleagueService.getCourseData();
  }

}
