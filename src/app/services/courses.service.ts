import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CoursesService {

  constructor(private http: HttpClient) {
  }

  public getCourseData(): Observable<any> {
    return this.http.get('http://localhost:8080/cursus-service/api/cursussen');
  }

}
